FROM openjdk:12
ADD target/docker-spring-boot-fssartori.jar docker-spring-boot-fssartori.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-fssartori.jar"]
